---
layout: markdown_page
title: Product Stage Direction - Plan
description: "The Plan stage enables teams to effectively plan features and projects in a single application"
canonical_path: "/direction/plan/"
---

Content last reviewed on 2024-05-14

- TOC
{:toc}

<p align="center">
    <font size="+2">
        <b>Enable teams to effectively plan and execute work in a single application</b>
    </font>
</p>

<%= partial("direction/plan/templates/overview") %>

<%= devops_diagram(["Plan"]) %>

## Stage Overview

The Plan Stage provides tools for teams to manage and optimize their work, track operational health and measure outcomes. As an end-to-end DevSecOps platform, GitLab is uniquely positioned to deliver a planning suite that enables business leaders to drive their vision and DevSecOps teams to deliver value while improving how they work. In addition, the unification of the DevSecOps process allows GitLab to interlink data across every stage of development, from ideation, to planning, implementation, deployment, and deliver value to customers.

### Group and Categories

<%= partial("direction/plan/templates/categories") %>

## 3 Year Stage Themes
<%= partial("direction/plan/templates/themes") %>
 
## 3 Year Strategy

In three years, the Plan Stage market will:
* Continue to shift from project to product and focus on outcomes instead of output.
* Continue to move away from command and control mentality and instead empower teams to determine how they can contribute toward business objectives.
* Make operational efficiency and continual improvement a top priority.  
* Embrace AI within the Plan stage of the DevSecOps toolchain and lifecycle.
* Shift toward consolidation into a single platform for all stages of the DevSecOps lifecycle.

As a result, in three years, Gitlab will:
* Provide support for individual DevSecOps teams and entire organizations using scaled Agile frameworks.
* Allow GitLab to capture and tie metrics to [Work Items](https://docs.gitlab.com/ee/development/work_items.html) to reflect business outcomes. 
* Surface metrics like DORA and Value Stream in key parts of a teams workflow to help drive improvements. 
* Support frameworks like OKRs that encourage bottom-up contributions. 
* Use downstream DevSecOps data for automation and AI to help teams improve their plans.
* Make it easy for non-Developer Personas to contribute in GitLab. 

## 1 Year Plan

### What We Recently Completed

<%= partial("direction/plan/project_management/team_planning/recent_accomplishments") %>
* Improvements to the rich text editor - Following the initial rollout in GitLab 16.2, we continue to make improvements to the rich text editor. In GitLab 16.9, the rich text editor was made available in [requirements descriptions](https://gitlab.com/gitlab-org/gitlab/-/issues/407493), [vulnerability findings](https://gitlab.com/gitlab-org/gitlab/-/issues/407491), [release descriptions](https://gitlab.com/gitlab-org/gitlab/-/issues/407494) and [design notes](https://gitlab.com/gitlab-org/gitlab/-/issues/407505).
* Increase wiki maturity - In [16.10](https://about.gitlab.com/releases/2024/03/21/gitlab-16-10-released/#wiki-templates), we introduced all-new templates to the Wiki to make your wiki page layouts more consistent, create or restructure pages faster, and ensure that information is presented clearly and coherently in your knowledge base. In 16.11, [autocomplete support for links to wiki pages](https://about.gitlab.com/releases/2024/04/18/gitlab-16-11-released/#autocomplete-support-for-links-to-wiki-pages), linking to wiki pages from your epics and issues has never been easier - it's just a matter of a few keystrokes.

### What We Are Currently Working On

* Migrating [epics to the work items](https://gitlab.com/groups/gitlab-org/-/epics/9290) framework will allow us to eventually bring more consistency with issues and address long-standing requests, like [assignees on epics](https://gitlab.com/groups/gitlab-org/-/epics/4231).
<%= partial("direction/plan/project_management/team_planning/current_focus") %>
* [Wiki Connectivity](https://gitlab.com/groups/gitlab-org/-/epics/12835) - Allow users to connect their Wiki knowledge base with the rest of the DevSecOps lifecycle. 
* [Pages Multi-Version Support](https://gitlab.com/groups/gitlab-org/-/epics/10914) - Currently a project can have only a single version of a GitLab Pages site. This make it hard for customers to try new ideas on their sites without changing the only version of the site. Customers need a way to preview changes or have multiple environments for their GitLab Pages sites to make it possible to validate changes before deploying their site.
* [ClickHouse readiness for General Availability](https://gitlab.com/groups/gitlab-org/-/epics/9281) - In FY23 ClickHouse was selected as GitLab's standard datastore for features with big data and insert-heavy requirements (e.g. Observability, Analytics, etc.). ClickHouse is not intended to replace Postgres or Redis in GitLab's stack. We are currently working in the implementation of the first report, [contribution analytics](https://gitlab.com/groups/gitlab-org/-/epics/11732), with this new datastore.  

<%= partial("direction/plan/templates/next") %>

### Cross-Stage Initiatives

Plan offers functionality that ties into workflows in other stages.  We are actively collaborating with other stages that are building upon Plan functionality to meet their users needs.

* The Manage:Import and Integrate group has built a Jira integration that displays Jira Issue data within GitLab. We will collaborate with that team to tie Jira Issues into more workflows like reporting and tying Jira Issues to higher level work items.
* The Manage:Import and Integrate owns the [Jira importer](https://docs.gitlab.com/ee/user/project/import/jira.html) to allow Jira issues to be migrated to GitLab. We will continue to work with that team to extend GitLab work items to accomodate more critical data elements from Jira to ensure a seamless import process.
* The Plan stage and the [Service Desk Single Engineer Group](https://about.gitlab.com/handbook/engineering/development/incubation/service-desk/) are collaborating on [accelerating Service Desk](https://gitlab.com/groups/gitlab-org/-/epics/8769), which will extend work items to support adjacent use cases to portfolio and team planning.
* Plan:Optimize is collaborating with Manage:organization to Consolidate [Value Stream Analytics Group & Project into a single object - Workspace](https://gitlab.com/groups/gitlab-org/-/epics/9295). 
* Plan:Optimize is collaborating with Analytics:Product Analytics to add [YML schema-driven customizable UI](https://gitlab.com/groups/gitlab-org/-/epics/8925) to the Value Streams Dashboard. 
* Plan:Optimize is collaborating with Govern:Threat Insights to add [Vulnerabilities metrics to the Value Streams Dashboard](https://gitlab.com/gitlab-org/gitlab/-/issues/383697).
* Plan:Knowledge collaborates with Groups across all areas of GitLab to develop and improve upon the rich text editor. The rich text editor is a what you see is what you get editing experience, and is an alternative to the plain text editor which requires knowledge of markdown to leverage.

### Target audience

<%= partial("direction/plan/templates/target_audience") %>

### Pricing

<%= partial("direction/plan/templates/pricing") %>

An example of what the end result data model and pricing could look like based on these pricing principles:

![Work Items Hierarchy](/images/direction/plan/workitemhierarchy.png)


### Jobs To Be Done 

[View the Plan stage JTBD](/direction/plan/jtbd.html)
